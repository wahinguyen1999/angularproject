import { Recipe } from './recipe.model';
import { EventEmitter, Injectable } from '@angular/core';
import { Ingredient } from './../shared/ingredient.model';
import { ShoppingListService } from './../shopping-list/shopping-list.service';

@Injectable({
    providedIn: 'root',
})

export class RecipeService {
    recipeSelected = new EventEmitter<Recipe>();

    private recipes: Recipe[] = [
        new Recipe('Mì xào Hàn Quốc', 'This is simply a test', 'https://assets.bonappetit.com/photos/5c2f8fe26558e92c8a622671/1:1/w_2700,h_2700,c_limit/bolognese-1.jpg',
            [
                new Ingredient('Mì', 1),
                new Ingredient('Japan', 2)
            ]),
        new Recipe('Cơm chiên', 'This is simply a test', 'https://assets.bonappetit.com/photos/5c2f8fe26558e92c8a622671/1:1/w_2700,h_2700,c_limit/bolognese-1.jpg',
            [
                new Ingredient('Cơm', 2),
                new Ingredient('Trứng', 3)
            ]),
        new Recipe('Gà tiềm', 'This is simply a test', 'https://assets.bonappetit.com/photos/5c2f8fe26558e92c8a622671/1:1/w_2700,h_2700,c_limit/bolognese-1.jpg',
            [
                new Ingredient('Gà', 3),
                new Ingredient('Nồi', 2)
            ]),
    ];
    constructor(private slService: ShoppingListService) { }
    getRecipes() {
        return this.recipes; //Return copy array instead of real array
    }
    addIngredientsToShoppingList(ingredients: Ingredient[]) {
        this.slService.addIngredients(ingredients);
    }
    getRecipe(index: number) {
        return this.recipes[index];
    }
    addRecipe(recipe: Recipe) {
        this.recipes.push(recipe);
    }
    updateRecipe(index: number, newRecipe: Recipe) {
        this.recipes[index] = newRecipe;
    }
    deleteRecipe(index: number) {
        this.recipes.splice(index, 1);
    }
}